* Two processors are connected with a wire (except the ground wire) are connected to each other 
the first processor's task is to get the numbers and the second processor's task is to show the numbers on 
7 segments . Write a program (a program for the first processor and a program for the second) to implement a 
2 digit calculator with four principle operations and ` + , - , * and / ` . We assume that the answer is not negative 
and doesnt have any floating point . The user enters the 2 digit number on the keypad and the number is shown simultaneously 
on the second processor's 7 segment . Then the operand is entered (there is no need to show the operand) and then the second 
number is entered ( the previous number is removed ) and then after entering the `=` button the result is printed . 
To transfer the data between 2 microprocessors (through only one line) you can use any protocol you want.

* It is suggested that this will be done through a PWM  wave or a square wave with CTC timer .



<img src="1.png" width="40%">
