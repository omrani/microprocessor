### Implemeting a Calculator with four principle operations for 2digit numbers 

#### On seven segment 

>  A program that simulates a Calculator on four 7 segments . The Calculator has 4 principle operations
> for 2 digit numbers . The user enters the first 2 digit number the number is shown on a 7 segments 
> then the operand is inserted but there is no need to show the operand on 7 segments . The the second 
> 2 digit number is inserted and is shown . At the end after clicking `=` button the result is shown . 
>  The user enters all the keys in a correct sequence and the Calculation is in the way that the result is always a normal number.



#### On LCD

> A program that simulates a 4 digit Calculator i.e the inputs have at most 4 digits .The Calculator has 4 principle
> operations on LCD. 
> The user can enter any number with any digit number up to 4 . All the inputs must be shown e.g 
> `3776-320 = 3456` must be shown completely. We assume that the result is a postivie normal number.
> The user must be able to correct the input phrase i.e he/she must be able to delete a part or the whole
> of the input phrase and replace a correct input instead.

